<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests;
use App\Http\Requests\StoreCategory;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends RootController
{

    
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.categories', compact('categories'));
    }



    public function create(Category $catM)
    {
        $categories = $catM->getCategories();
        $category = null;
        return view('admin.categories.category', compact('category', 'categories'));
    }



    public function store(StoreCategory $request, Category $catM)
    {
        $this->validate($request, [
            'slug' => 'required|unique:posts|min:2|max:255',
        ]);

        $category = $request->only('title', 'seo_title', 'seo_keywords', 'seo_description', 'category_id');
        $category['slug'] = str_slug($request->get('slug'));
        !empty($request->get('active')) ? $category['active'] = 1 : null;
        $category['user_id'] = Auth::user()->id;

        $catM->store($category);
        return redirect()->route('categories.all');
    }



    public function show($id)
    {
        //
    }


    public function edit($slug, Category $catM)
    {
        $category = $catM->getCategoryBySlug($slug);
        $categories = $catM->getCategories();
        return view('admin.categories.category', compact('category', 'categories'));
    }



    public function update(StoreCategory $request, Category $categoryM)
    {
        $category = $request->only(['title', 'seo_title', 'seo_keywords', 'seo_description', 'category_id']);
        $slug = $request->get('slug');
        !empty($request->get('active')) ? $category['active'] = 1 : null;
        $category['user_id'] = Auth::user()->id;
        $categoryM->updates($slug, $category);
        return redirect()->route('categories.all');

    }



    public function destroy($slug, Category $catM)
    {
        $catM->remove($slug);
        return redirect()->route('categories.all');
    }
}
