<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class PermissionController extends RootController
{

    public function index()
    {
        $permissions = Permission::all();
        return view('admin.users.permissions', compact('permissions'));
    }


    public function create()
    {
        return view('admin.users.permission');
    }


    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'unique:permissions|required']);

        Permission::create([
            'name' => $request->get('name'),
            'display_name' => $request->get('display_name'),
            'description' => $request->get('description'),
        ]);

        return redirect()->route('permissions.all');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }



    public function update(Request $request, Permission $permM, $id)
    {
        $permission = $request->only(['display_name', 'description']);
        $permM->updates($id, $permission);
        return redirect()->route('permissions.all');
    }
    
    
    
    public function destroy($id)
    {
        $perm = Permission::getById($id);
        $perm->delete();
        return redirect()->route('permissions.all');
    }
}
