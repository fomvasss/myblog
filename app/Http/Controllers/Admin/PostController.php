<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StorePost;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Category;

class PostController extends RootController
{



    public function index(Post $data)
    {
        $posts = $data->getPosts();
        $seo['title'] = $this->seo['title'];
        return view('admin.posts.posts', compact(['posts', 'seo']));
    }


    public function create(Category $categoriesM)
    {
        $categories = $categoriesM->getCategories();
        $post = null;
        return view('admin.posts.post', compact('categories', 'post'));
    }


    public function store(StorePost $request, Post $postM)
    {
        $this->validate($request, [
            'slug' => 'required|unique:posts|min:2|max:255',
        ]);

        $post = $request->only(['title', 'img', 'description', 'content', 'category_id', 'published_at', 'seo_title', 'seo_description', 'seo_keywords']);

        $post['slug'] = str_slug($request->get('slug'));
        !empty($request->get('active')) ? $post['active'] = 1 : null;

        $post['user_id'] = Auth::user()->id;
        $postM->saves($post);
        return redirect()->route('posts.all');
    }


    public function show($id, Post $posts)
    {
        dd (($posts->getPostsByCategoryId($id)));
    }


    public function edit($slug, Post $postM, Category $categoriesM)
    {
        $categories = $categoriesM->getCategories();
        $post = $postM->getPostBySlug($slug);

        return response()->view('admin.posts.post', compact('post', 'categories'));

//        return view('admin.posts.post', compact('post', 'categories'));
    }

    public function update(StorePost $request, Post $postM)
    {
//        dd($request->get('published_at')); return;
        $data = $request->only(['title', 'img', 'description', 'content', 'category_id', 'published_at', 'seo_title', 'seo_description', 'seo_keywords']);
        $slug = $request['slug'];
        !empty($request->get('active')) ? $data['active'] = 1 : null;
//        $data['published_at'] = Carbon::now();
        $data['user_id'] = Auth::user()->id;
        
        $postM->updates($slug, $data);
        return redirect()->route('posts.all');
    }


    public function destroy(Request $request, Post $post, $slug)
    {
//        $slug = $request->get('slug');
        $post->remove($slug);
        return redirect()->route('posts.all');
    }
}
