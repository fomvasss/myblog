<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Zizaco\Entrust\Entrust;

class RoleController extends RootController
{

    public function index(Request $request)
    {
        Log::info('Showing user profile for user: ');
//        dd($request->session()->all());
//        if (Auth::user()->can(['role-list'])) {
//        if ($this->getUser()->can(['role-list'])) { //test use role
            $roles = Role::all();
            return view('admin.users.roles', compact('roles'));
//        }
//        abort(403);
        return response(view('errors.403'), 403);
    }

    public function create()
    {

        $permissions = Permission::all();
        return view('admin.users.role', compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'unique:roles|required|min:2|max:255',
            'display_name' => 'required|min:2|max:255',
            'description' => 'max:255',
        ]);
/*
//Method 1 saved
        $owner = new Role();
        $owner->name         = 'owner13';
        $owner->display_name = 'Project Owner3'; // optional
        $owner->description  = 'User3 is the owner of a given project'; // optional
        $owner->save();

//Method 2 saved
        $roleM = new Role();
        $role = $request->only(['name', 'display_name', 'description']);
        $roleM->saves($role);
*/
//Method 3 saved
        Role::create([
            'name' => $request->get('name'),
            'display_name' => $request->get('display_name'),
            'description' => $request->get('description'),
        ]);

//Get role with name $request->get('name')
        $permRole = Role::where('name', '=', $request->get('name'))->first();

//Set permission for role with var name $permRole
        $permRole->perms()->sync($request->get('permission'));

        return redirect()->route('roles.all');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $role = Role::getById($id); // Pull back a given role
        
        $role->delete();
        return redirect()->route('roles.all');

    }
}
