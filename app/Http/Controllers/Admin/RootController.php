<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;


class RootController extends Controller
{
	protected $seo = [];

	public function __construct()
	{
		$this->middleware('auth');

		$this->seo['title'] = 'Admin Panel';

//		dd($this->getUser());

//		$this->user = (Auth::user()->name); dd($this->user);
//		$this->user = User::where('name', '=', 'SuperAdmin')->first();dd($this->user);

	}

	protected function getUser()
	{
//		return $this->app->auth->user();
		return Auth::user();
	}
}
