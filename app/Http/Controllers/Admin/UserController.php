<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;


class UserController extends RootController
{

    public function index()
    {
        $users = User::all();
        $roles = Role::all();
//        $roles = [];
//        foreach ($users as $i=>$user) {
//            $roles[$user['id']] = User::find($i)->roles()->get();
//        }
//        return view('admin.users.users', compact('users', 'roles'));

        return response()->view('admin.users.users', compact('users', 'roles'))->withCookie('name1', '123');
    }



    public function create()
    {
        $roles = Role::all(); 
        return view('admin.users.user', compact('roles'));
    }



    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'min:6|confirmed',
            'role' => 'required'
        ]);

//        dd(array_values($request->get('role')));
//        return;

        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        $user = User::where('email', '=', $request->get('email'))->first();

        foreach ($request->get('role') as $role) {
            $user->roles()->attach($role); //id user or $user->attachRole(1);
        }
        return redirect()->route('users.all');
    }


    public function show($id = null)
    {
        //
    }


    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        $roles = Role::all();
        return view('admin.users.user', compact(['user', 'roles']));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'password' => 'min:6|confirmed',
            'role' => 'required'
        ]);

        $user = $request->only('name', 'email');
        if ($request['password'] != '') $user['password'] = $request->get('password');
        User::where('id', $id)->update($user);
        $user = User::where('email', '=', $request->get('email'))->first();

        $user->roles()->sync($request->get('role'));

        return redirect()->route('users.all');
    }


    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->route('users.all');
    }
}
