<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePost extends FormRequest
{

    protected $redirect = '/admin/posts';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|max:255',
            'img' => 'max:10000',
            'description' => 'required',
            'category_id' => 'numeric',
            'date' => 'date',
            'tags' => 'max:255',
            'seo_title' => 'max:255',
            'seo_description' => 'max:255',
            'seo_keywords' => 'max:255',
        ];
    }

    public function messages()
    {
        return [
          'title.required' => 'Название обязательно для заполнения',
          'title.min' => 'Название должно содержать минимум 2 символа',
          'title.max' => 'Название должно содержать максимум 255 символов',
        ];
    }
}
