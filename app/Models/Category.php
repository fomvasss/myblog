<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'categories';

	public $timestamps = false;


	public function post()
	{
		return $this->hasMany('Post', 'category_id');
	}


	public function store($category)
	{
		Category::insert($category);
	}

	public function updates($slug, $category)
	{
		Category::bySlug($slug)->update($category);
	}

	public function remove($slug)
	{
//		Post::where('category_id', $this->id)->delete();
		Category::bySlug($slug)->delete($slug);
	}

	public function getCategories()
	{
		return Category::all();
	}

	public function getCategoryBySlug($slug)
	{
		return Category::bySlug($slug)->first();
	}


	public function scopeBySlug($query, $slug)
	{
		return $query->where('slug', $slug);
	}

}
