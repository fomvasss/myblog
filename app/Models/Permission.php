<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{

	public $table = 'permissions';

	public $timestamps = true;

	protected $fillable = [
		'name', 'display_name', 'description',
	];

	public function getPerms()
	{
		return Permission::all();
	}

	public function updates($id, $perm)
	{
		Permission::byId($id)->update($perm);
	}


	static function getById($id)
	{
		return Permission::byId($id);
	}

	public function scopeById($query, $id)
	{
		return $query->where('id', $id);
	}

}
