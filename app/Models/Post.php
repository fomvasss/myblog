<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'posts';

	protected $fillable = ['slug','title', 'img', 'description', 'content', 'category_id', 'published_at', 'seo_title', 'seo_description', 'seo_keywords'];
	
	public $timestamps = true;

		/** Relations **/
	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}
	
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	

		/** Queries **/
	public function saves($post)
	{
		Post::insert($post);
	}

	public function updates($slug, $post)
	{
		Post::bySlug($slug)->update($post);
	}

	public function remove($slug)
	{
		Post::bySlug($slug)->delete();
	}

	public function getPosts()
	{
		return Post::all();
	}

	public function getPostBySlug($slug)
	{
		return Post::bySlug($slug)->first();
	}

	public function getPostsByCategoryId($id)
	{
		return Post::where('category_id', $id)->get();
	}


		/** Scopes  **/
	public function scopeBySlug($query, $slug)
	{
		return $query->where('slug', $slug);
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}



	/*public function setSlugAttribute($value)
{
	$this->attributes['slug'] = str_slug($value);
}*/

}
