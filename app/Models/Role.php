<?php

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{

	public $table = 'roles';

	public $timestamps = true;

	protected $fillable = [
		'name', 'display_name', 'description',
	];


//	public function users()
//	{
//		return $this->belongsToMany('App\Models\User', 'role_user');
//	}


	public function saves($role)
	{
		Role::insert($role);
	}

	public function updates($id, $role)
	{
		Role::byId($id)->update($role);
	}

	static function getById($id)
	{
		return Role::byId($id);
	}
	
	public function scopeById($query, $id)
	{
		return $query->where('id', $id);
	}




}
