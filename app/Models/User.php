<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

//
//    public function roles()
//    {
//        return $this->belongsToMany('App\Models\Role', 'role_user');
//    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function getUsers()
    {
        return User::all();
    }

    static function destroy($id)
    {
        User::byId($id)->delete();
    }


    public function scopeById($query, $id)
    {
        return $query->where('id', $id);
    }

}
