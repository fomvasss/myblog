<?php


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'),
//        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->sentence,
        'description' => $faker->text,
        'content' => $faker->text,
        'img' => $faker->image(public_path().'/uploads/img', 360, 360, null, false),
        'seo_title' => $faker->sentence,
        'seo_keywords' => $faker->sentence,
        'seo_description' => $faker->sentence,
        'active' => rand(1, 2),
        'published_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'user_id' => $faker->numberBetween(1, 4),
        'category_id' => $faker->numberBetween(1, 2),
    ];
});

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->sentence,
        'seo_title' => $faker->text,
        'seo_keywords' => $faker->text,
        'seo_description' => $faker->text,
        'active' => $faker->boolean(),
        'user_id' => $faker->numberBetween(1, 10),
        'category_id' => $faker->numberBetween(0, 4),
    ];
});

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [
        'text' => $faker->text,
        'user_id' => $faker->numberBetween(1, 10),
        'post_id' => $faker->numberBetween(1, 20),
    ];
});

