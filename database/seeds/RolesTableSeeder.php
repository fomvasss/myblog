<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use Zizaco\Entrust;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Administrator',
            'description' => 'User is allowed to manage and edit other users, settings site, contents'
        ]);
        DB::table('roles')->insert([
            'name' => 'editor',
            'display_name' => 'Editor',
            'description' => 'User is allowed to add posts, pages, edit own content'
        ]);
        DB::table('roles')->insert([
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'User is allowed to leave comments'
        ]);
        DB::table('roles')->insert([
            'name' => 'guest',
            'display_name' => 'Guest',
            'description' => 'User is allowed views sites'
        ]);

        $admin = User::where('id', '=', '1')->first();
        $admin->attachRole(1);

//Get role with name $request->get('name')
        $permRole = Role::where('name', '=', 'admin')->first();
//Set permission for role with var name $permRole
        $perms = Permission::all();
        foreach ($perms as $perm) {
            $permRole->attachPermissions(array($perm['id']));
        }

    }
}
