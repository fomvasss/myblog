<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'First Admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('123456'),
        ]);

        factory(App\Models\User::class, 10)->create();
    }
}
