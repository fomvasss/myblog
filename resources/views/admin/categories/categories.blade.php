@extends('admin.layouts.app')

@section('content')
	<div id="wrapper">
		<div id="page-wrapper">
			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							Категории сайта<small></small>
						</h1>
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
							</li>
							<li class="active">
								<i class="fa fa-edit"></i> Все категории
							</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->


				<div class="row">
					<div class="col-lg-12">

						<!-- .table-responsive -->
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
								<tr>
									<th>Название</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								@foreach($categories as $category)
									<tr>
										<td><a href="{{ route('category.edit', $category['slug']) }}">{{$category['title'] or ''}}</a></td>
										<td><a href="{{ route('category.edit', $category['slug']) }}"><span class="fa fa-pencil-square"></span></a> </td>
										<td><a href="#"><span class="fa fa-eye"></span></a> </td>
										<td><a href="{{ route('category.destroy', $category['slug']) }}"><span class="fa fa-trash-o"></span></a> </td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->

					</div>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
@stop