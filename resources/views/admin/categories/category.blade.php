@extends('admin.layouts.app')

@section('content')
	<div id="wrapper">
		<div id="page-wrapper">
			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
							</li>
							<li>
								<i class="fa fa-pencil-square-o"></i>  <a href="/admin/categories">Категории</a>
							</li>
							<li class="active">
								@if (empty($category))
									<i class="fa fa-edit"></i> Создать
								@else
									<i class="fa fa-edit"></i> Редактировать
								@endif
							</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->


				<div class="row">
					<div class="col-lg-12">

					@include('admin.vendor.errors')

					<!-- post -->
						@if (empty($category))
							<form action="{{ route('category.store') }}" method="POST">
								@else
							<form action="{{ url('/admin/categories/update',$category['slug']) }}" method="POST">
							{{--<form action="{{ route('category.update', $category['slug']) }}" method="POST">--}}
								@endif
								{{ csrf_field() }}
								<div class="form-group">
									<label>Название статьи</label>
									<input type="text" class="form-control" name="title" required value="{{ $category['title'] or '' }}">
								</div>

								<div class="form-group">
									<label>Слаг категории</label>
									@if (empty($category))
										<input type="text" class="form-control" name="slug" required>
									@else
										<input type="text" class="form-control" name="slug" required readonly value="{{ $category['slug'] or '' }}">
									@endif
								</div>

								<div class="form-group">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="active" @if (empty($category) || !empty($category['active'])) checked @endif>Публиковать категорию
										</label>
									</div>
								</div>

								<div class="form-group">
									<label>Подкатегория</label>
									<select class="form-control" name="category_id">
										<option value="0" selected>-</option>
										@foreach($categories as $cat)
											@if ($category['category_id'] == $cat['id'])
												<option value="{{ $cat['id'] }}" selected>{{ $cat['title'] }}</option>
											@else
												<option value="{{ $cat['id'] }}">{{ $cat['title'] }}</option>
											@endif
										@endforeach
									</select>
								</div>

								<div class="form-group">
									<label>SEO-название</label>
									<input type="text" class="form-control" name="seo_title" value="{{ $category['seo_title'] or '' }}">
								</div>

								<div class="form-group">
									<label>SEO-описание</label>
									<input type="text" class="form-control" name="seo_description" value="{{ $category['seo_description'] or '' }}">
								</div>

								<div class="form-group">
									<label>SEO-ключевые слова</label>
									<input type="text" class="form-control" name="seo_keywords" value="{{ $category['seo_keywords'] or '' }}">
								</div>

								<div class="text-right">
									<button type="reset" class="btn btn-default">Очистить</button>
									<button type="submit" class="btn btn-primary">Сохранить</button>
								</div>

							</form>
							<!-- /post -->

					</div>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
@stop