<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	{{--<title>SB Admin - Bootstrap Admin Template</title>--}}
	@include('admin.vendor.seo')

	<!-- Bootstrap Core CSS -->
	<link href="{{ asset('admin-assets/css/bootstrap.min.css') }}" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="{{ asset('admin-assets/css/sb-admin.css') }}" rel="stylesheet">

	{{--<!-- Morris Charts CSS -->--}}
	{{--<link href="{{ asset('admin-assets/css/plugins/morris.css') }}" rel="stylesheet">--}}

	<!-- Custom Fonts -->
	<link href="{{ asset('admin-assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>



@if (!Auth::guest())
	@include('admin.vendor.nav')
@endif

	@yield('content')


<!-- jQuery -->
<script src="{{ asset('admin-assets/js/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('admin-assets/js/bootstrap.min.js') }}"></script>
{{--<!-- Morris Charts JavaScript -->--}}
{{--<script src="js/plugins/morris/raphael.min.js"></script>--}}
{{--<script src="js/plugins/morris/morris.min.js"></script>--}}
{{--<script src="js/plugins/morris/morris-data.js"></script>--}}

</body>

</html>
