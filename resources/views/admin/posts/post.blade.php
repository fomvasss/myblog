@extends('admin.layouts.app')

@section('content')
<div id="wrapper">
	<div id="page-wrapper">
		<div class="container-fluid">

			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
						</li>
						<li>
							<i class="fa fa-pencil-square-o"></i>  <a href="/admin/posts">Статьи</a>
						</li>
						<li class="active">
							@if (empty($post))
							<i class="fa fa-edit"></i> Создать
							@else
							<i class="fa fa-edit"></i> Редактировать
							@endif
						</li>
					</ol>
				</div>
			</div>
			<!-- /.row -->


			<div class="row">
				<div class="col-lg-12">

					@include('admin.vendor.errors')

					<!-- post -->
						@if (empty($post))
						<form action="{{ route('post.store') }}" method="POST">
						@else
						<form action="{{ route('post.update', $post['slug']) }}" method="POST">
						@endif
								{{ csrf_field() }}
							<div class="row">
								<div class="col-md-10">
									<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
										<label>Название статьи</label>
										<input type="text" class="form-control" name="title" required value="{{ $post['title'] or '' }}">
									</div>

									<div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
										<label>Слаг статьи</label>
										@if (empty($post))
											<input type="text" class="form-control" name="slug" required>
										@else
											<input type="text" class="form-control" name="slug" required readonly value="{{ $post['slug'] or '' }}">
										@endif
									</div>

								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>File input</label>
										<input type="file" name="img" accept="image/*">
										<img class="post img-thumbnail" src="/uploads/img/{{$post['img']}}" alt="">
									</div>
								</div>
							</div>

							<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
								<label>Описание статьи</label>
								<textarea class="form-control" rows="4" name="description">{{ $post['description'] or '' }}</textarea>
							</div>

							<div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
								<label>Контент статьи</label>
								<textarea class="form-control" rows="6" name="content">{{ $post['content'] or '' }}</textarea>
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="active" @if (empty($post) || !empty($post['active'])) checked @endif>Публиковать статью
									</label>
								</div>
							</div>

							<div class="form-group {{ $errors->has('published_at') ? ' has-error' : '' }}">
								<label>Дата публикации</label>

								<input type="date" class="form-control" name="published_at" id="datePicker" style="width:200px;" value="@datetime($post['published_at'])">
								@unless($post)
								<script>document.getElementById('datePicker').valueAsDate = new Date();</script>
								@endunless
							</div>

							<div class="form-group">
								<label>Категория</label>
								<select class="form-control" name="category_id" style="width:200px;">
									<option value="0" selected>-</option>
									@foreach($categories as $cat)
										@if ($post['category_id'] == $cat['id'])
											<option value="{{ $cat['id'] }}" selected>{{ $cat['title'] }}</option>
										@else
											<option value="{{ $cat['id'] }}">{{ $cat['title'] }}</option>
										@endif
									@endforeach
								</select>
							</div>

							<div class="form-group{{ $errors->has('seo_title') ? ' has-error' : '' }}">
								<label>SEO-название</label>
								<input type="text" class="form-control" name="seo_title" value="{{ $post['seo_title'] or '' }}">
							</div>

							<div class="form-group {{ $errors->has('seo_description') ? ' has-error' : '' }}">
								<label>SEO-описание</label>
								<input type="text" class="form-control" name="seo_description" value="{{ $post['seo_description'] or '' }}">
							</div>

							<div class="form-group {{ $errors->has('seo_keywords') ? ' has-error' : '' }}">
								<label>SEO-ключевые слова</label>
								<input type="text" class="form-control" name="seo_keywords" value="{{ $post['seo_keywords'] or '' }}">
							</div>

							<div class="text-right">
								<button type="reset" class="btn btn-default">Очистить</button>
								<button type="submit" class="btn btn-primary">Сохранить</button>
							</div>

						</form>
					<!-- /post -->

				</div>
			</div>
			<!-- /.row -->

		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
@stop