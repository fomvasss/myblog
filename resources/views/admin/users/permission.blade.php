@extends('admin.layouts.app')

@section('content')
	<div id="wrapper">
		<div id="page-wrapper">
			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
							</li>
							<li class="active">
								<i class="fa fa-edit"></i> Права
							</li>
							<li class="active">
								<i class="fa fa-edit"></i> Создать
							</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->


				<div class="row">
					<div class="col-lg-12">

						<!-- post -->
						<form method="POST" action="{{ route('permission.store') }}">
							{{ csrf_field() }}
							<div class="form-group  ">
								<label class="control-label">Name for permission</label>
								<input class="form-control" name="name" type="text" placeholder="Example: create-post" value="">
							</div>
							<div class="form-group  ">
								<label class="control-label">Display name for permission</label>
								<input class="form-control" name="display_name" type="text" placeholder="Example: Create Posts">
							</div>
							<div class="form-group  ">
								<label class="control-label">Description for permission</label>
								<input class="form-control" name="description" type="text" placeholder="Example: Create Posts description">
							</div>
							<div class="text-right">
								<button type="reset" class="btn btn-default">Очистить</button>
								<button type="submit" class="btn btn-primary">Сохранить</button>
							</div>
						</form>

						<!-- /post -->

					</div>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
@stop