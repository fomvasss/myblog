@extends('admin.layouts.app')

@section('content')
	<div id="wrapper">
		<div id="page-wrapper">
			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">

						<ol class="breadcrumb">
							<li>
								<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
							</li>
							<li class="active">
								<i class="fa fa-edit"></i> Права
							</li>
							<li class="active">
								<i class="fa fa-edit"></i> Все
							</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->


				<div class="row">
					<div class="col-lg-12">

						<!-- post -->

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
								<tr>
									<th>Название в системе</th>
									<th>Название на сайте</th>
									<th>Описание</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								@foreach($permissions as $perm)
									<tr>
										<td><a href="#">{{$perm['name'] or ''}}</a></td>
										<td>{{$perm['display_name'] or ''}}</td>
										<td>{{$perm['description'] or ''}}</td>
										<td><a href="#"><span class="fa fa-pencil-square"></span></a> </td>
										<td><a href="#"><span class="fa fa-eye"></span></a> </td>
										<td><a href="{{ route('permission.destroy', $perm['id']) }}"><span class="fa fa-trash-o"></span></a> </td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>

						<!-- /post -->

					</div>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
@stop