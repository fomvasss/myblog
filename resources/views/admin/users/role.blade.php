@extends('admin.layouts.app')

@section('content')
	<div id="wrapper">
		<div id="page-wrapper">
			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							Роли <small>просмотр</small>
						</h1>
						<ol class="breadcrumb">
							<li>
								<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
							</li>
							<li class="active">
								<i class="fa fa-edit"></i> Создать
							</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->


				<div class="row">
					<div class="col-lg-12">
						@include('admin.vendor.errors')
						<!-- post -->
						<form method="POST" action="{{ route('role.store') }}">
							{{ csrf_field() }}
							<div class="form-group  ">
								<label class="control-label">Name for role</label>
								<input class="form-control" name="name" type="text" placeholder="admin">
							</div>
							<div class="form-group  ">
								<label class="control-label">Display name for role</label>
								<input class="form-control" name="display_name" type="text" placeholder="Administrator">
							</div>
							<div class="form-group  ">
								<label class="control-label">Description for role</label>
								<input class="form-control" name="description" type="text" placeholder="Administrator desc">
							</div>

							<div class="form-group">
								<label>Permissions for role</label>
								@foreach($permissions as $perm)
								<div class="checkbox">
									<label>
										<input type="checkbox" name="permission[]" value="{{ $perm['id'] }}">{{ $perm['name'] }}
									</label>
								</div>
								@endforeach
							</div>


							<div class="text-right">
								<button type="reset" class="btn btn-default">Очистить</button>
								<button type="submit" class="btn btn-primary">Сохранить</button>
							</div>
						</form>

						<!-- /post -->

					</div>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
@stop