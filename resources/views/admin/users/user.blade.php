@extends('admin.layouts.app')

@section('content')
<div id="wrapper">
	<div id="page-wrapper">
		<div class="container-fluid">

			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">

					<ol class="breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
						</li>
						<li class="active">
							<i class="fa fa-edit"></i> Пользователи
						</li>
						<li class="active">
							<i class="fa fa-edit"></i> Создать
						</li>
					</ol>
				</div>
			</div>
			<!-- /.row -->


			<div class="row">
				<div class="col-lg-12">
					@include('admin.vendor.errors')
					<!-- post -->
				@if (empty($user))
					<form method="POST" action="{{ route('user.store') }}" accept-charset="UTF-8">
				@else
					<form method="POST" action="{{ route('user.update', $user['id']) }}" accept-charset="UTF-8">
				@endif
						{{ csrf_field() }}
						<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="active" @if (empty($user) || !empty($user['active'])) checked @endif>Активный
									</label>
								</div>
						</div>
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="control-label">Имя</label>
							<input class="form-control"  placeholder="" name="name" type="text" value="{{ $user['name'] or '' }}">
						</div>

						<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
							<label class="control-label">Email</label>
							<input class="form-control" placeholder="" name="email" type="email" value="{{ $user['email'] or '' }}">

						</div>

						<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
							<label class="control-label">Пароль</label>
							<input class="form-control" placeholder="" name="password" type="password">
						</div>

						<div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<label class="control-label">Подтвердите пароль</label>
							<input class="form-control" placeholder="" name="password_confirmation" type="password">
						</div>

						{{--<div class="form-group">--}}
							{{--<label class="control-label">Роль</label>--}}
							{{--<select class="form-control" name="role">--}}
								{{--@foreach($roles as $role)--}}
								{{--<option value="{{ $role['id'] }}">{{ $role['name'] }}</option>--}}
								{{--@endforeach--}}
							{{--</select>--}}
						{{--</div>--}}

						<div class="form-group">
							<label>Роли</label>
							@foreach($roles as $role)
							<div class="checkbox">
								<label>
									<input type="checkbox" name="role[]" value="{{ $role['id'] }}" @if (!empty($user) && $user->hasRole($role['name'])) checked @endif>{{ $role['display_name'] }}
								</label>
							</div>
							@endforeach
						</div>

							<div class="text-right">
								<button type="reset" class="btn btn-default">Очистить</button>
								<button type="submit" class="btn btn-primary">Сохранить</button>
							</div>

						</form>
					<!-- /post -->

				</div>
			</div>
			<!-- /.row -->

		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
@stop