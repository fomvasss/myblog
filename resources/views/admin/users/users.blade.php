@extends('admin.layouts.app')

@section('content')
<div id="wrapper">
	<div id="page-wrapper">
		<div class="container-fluid">

			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">

					<ol class="breadcrumb">
						<li>
							<i class="fa fa-dashboard"></i>  <a href="/admin">Dashboard</a>
						</li>
						<li class="active">
							<i class="fa fa-edit"></i> Пользователи
						</li>
						<li class="active">
							<i class="fa fa-edit"></i> Все пользователи
						</li>
					</ol>
				</div>
			</div>
			<!-- /.row -->


			<div class="row">
				<div class="col-lg-12">

					<div id="tri" class="btn-group btn-group-sm">
						<a href="#" role="button" class="btn btn-default ">All
							<span class="badge">4</span>
						</a>
						<a href="#" role="button" class="btn btn-default ">Administrators
							<span class="badge">1</span>
						</a>
						<a href="#" role="button" class="btn btn-default ">Redactors
							<span class="badge">1</span>
						</a>
						<a href="#" role="button" class="btn btn-default ">Users
							<span class="badge">2</span>
						</a>
					</div>

					<!-- .table-responsive -->
					{{--@if (Entrust::hasRole('owner9'))--}}
					{{--123 \Entrust::role('admin')--}}
					{{--@endif--}}

					{{--@role(('owner9'))--}}
					@role('owner9')
						123213123ssssssssssssssssssssssssssssssssss123
					@endrole

					@role('12')
					123213123ssssssssssssssssssssssssssssssssss123
					@endrole

					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
							<tr>
								<th>Имя</th>
								<th>Роль</th>
								<th>Активность</th>
								<th></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							@foreach($users as $i=>$user)
								<tr>
								<td><a href="{{ $user['id'] }}">{{ $user['name'] }}</a></td>
								<td>
									@foreach($roles as $role)
										@if ($users[$i]->hasRole($role['name']))
											{{ $role['display_name'] }}
										@endif
									@endforeach
								</td>
								<td><input name="active" type="checkbox" checked value="1"></td>
								<td><a href="{{ route('user.edit', $user['id']) }}"><span class="fa fa-pencil-square"></span></a> </td>
								<td><a href="#"><span class="fa fa-eye"></span></a> </td>
								<td><a href="{{ route('user.destroy', $user['id']) }}"><span class="fa fa-trash-o"></span></a> </td>
							</tr>
							@endforeach

							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
					
				</div>
			</div>
			<!-- /.row -->

		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
@stop