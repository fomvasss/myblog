<!DOCTYPE html>
<html>
<head>
    <title>Acces forbidden. Error code 403</title>
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <p>Error code 403</p>
        <div class="title"><h2>Acces forbidden</h2></div>
        <a href="/">Go to index</a>
    </div>
</div>
</body>
</html>
