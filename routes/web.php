<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Models\Role;

//Route::get('/admin/posts/show/{id}', ['uses' => 'Admin\PostController@show', 'as' => 'posts.show']);
//Route::group([ 'middleware' => ['permission:role-list']], function() {
	Route::get('/admin/posts', ['uses' => 'Admin\PostController@index', 'as' => 'posts.all']);
	Route::get('/admin/posts/create', ['uses' => 'Admin\PostController@create', 'as' => 'post.create']);
	Route::post('/admin/posts/store', ['uses' => 'Admin\PostController@store', 'as' => 'post.store']);
	Route::get('/admin/posts/edit/{slug}', ['uses' => 'Admin\PostController@edit', 'as' => 'post.edit']);
	Route::post('/admin/posts/update/{slug}', ['uses' => 'Admin\PostController@update', 'as' => 'post.update']);
	Route::get('/admin/posts/destroy/{slug}', ['uses' => 'Admin\PostController@destroy', 'as' => 'post.destroy']);
//});
Route::get('/admin/categories', ['uses' => 'Admin\CategoryController@index', 'as' => 'categories.all']);
Route::get('/admin/categories/create', ['uses' => 'Admin\CategoryController@create', 'as' => 'category.create']);
Route::post('/admin/categories/store', ['uses' => 'Admin\CategoryController@store', 'as' => 'category.store']);
Route::get('/admin/categories/edit/{slug}', ['uses' => 'Admin\CategoryController@edit', 'as' => 'category.edit']);
Route::post('/admin/categories/update/{slug}', ['uses' => 'Admin\CategoryController@update', 'as' => 'category.update']);
Route::get('/admin/categories/destroy/{slug}', ['uses' => 'Admin\CategoryController@destroy', 'as' => 'category.destroy']);

Route::get('/admin/users', ['uses' => 'Admin\UserController@index', 'as' => 'users.all']);
Route::get('/admin/users/create', ['uses' => 'Admin\UserController@create', 'as' => 'user.create']);
Route::post('/admin/users/store', ['uses' => 'Admin\UserController@store', 'as' => 'user.store']);
Route::get('/admin/users/show/{id?}', ['uses' => 'Admin\UserController@show', 'as' => 'user.show']);
Route::get('/admin/users/edit/{id}', ['uses' => 'Admin\UserController@edit', 'as' => 'user.edit']);
Route::post('/admin/users/update/{id}', ['uses' => 'Admin\UserController@update', 'as' => 'user.update']);
Route::get('/admin/user/destroy/{id}', ['uses' => 'Admin\UserController@destroy', 'as' => 'user.destroy']);



Route::get('/admin/roles', ['uses' => 'Admin\RoleController@index', 'as' => 'roles.all']);
Route::get('/admin/roles/create', ['uses' => 'Admin\RoleController@create', 'as' => 'role.create']);
Route::post('/admin/roles/store', ['uses' => 'Admin\RoleController@store', 'as' => 'role.store']);
Route::get('/admin/roles/destroy/{id}', ['uses' => 'Admin\RoleController@destroy', 'as' => 'role.destroy']);


Route::get('/admin/permissions', ['uses' => 'Admin\PermissionController@index', 'as' => 'permissions.all']);
Route::get('/admin/permissions/create', ['uses' => 'Admin\PermissionController@create', 'as' => 'permission.create']);
Route::post('/admin/permissions/store', ['uses' => 'Admin\PermissionController@store', 'as' => 'permission.store']);
Route::get('/admin/permissions/destroy/{id}', ['uses' => 'Admin\PermissionController@destroy', 'as' => 'permission.destroy']);

Route::get('/admin', 'Admin\AdminController@index');


Route::get('/', 'IndexController@index');


Auth::routes();

Route::get('/home', 'HomeController@index');

